import random
d = {
'Arcane1' : ['Magic Missile', 'Mage Armor', 'Shield'],
'Arcane2' : ['Scorching Ray', 'Acid Arrow'],
'Arcane3' : ['Fireball', 'Haste'],
'Arcane4' : ['Testa4'],
'Arcane5' : ['Testa5'],
'Arcane6' : ['Testa6'],
'Arcane7' : ['Testa7']
'Arcane8' : ['Testa8'],
'Arcane9' : ['Testa9'],
'Divine1' : ['Cure Light Wounds', 'Divine Favor', 'Bless'],
'Divine2' : ['Cure Moderate Wounds', 'Bulls Strength'],
'Divine3' : ['Cure Serious Wounds', 'Righteous Might'],
'Divine5' : ['Testd5'],
'Divine6' : ['Testd6'],
'Divine7' : ['Testd7'],
'Divine8' : ['Testd8'],
'Divine9' : ['Testd9']
}
ArcaneDivine = ["Arcane", "Divine"]
ArcaneDivineRand = random.choice(ArcaneDivine)
SpellLevel = random.randint(1,9)
SpellLevelstr = str(SpellLevel)
spell = (ArcaneDivineRand+SpellLevelstr)
RandomSpell = random.choice(d[spell])
if SpellLevel == 1:
    CasterLevel = 1
elif SpellLevel == 2:
    CasterLevel = 3
elif SpellLevel == 3:
    CasterLevel = 5
elif SpellLevel == 4:
    CasterLevel = 7
elif SpellLevel == 5:
    CasterLevel = 9
elif SpellLevel == 6:
    CasterLevel = 11
elif SpellLevel == 7:
    CasterLevel = 13
elif SpellLevel == 8:
    CasterLevel = 15
else:
    CasterLevel = 17
price = 25*CasterLevel*SpellLevel
print("You got a {} scroll of {} and it is worth {} gold".format(ArcaneDivineRand,RandomSpell,price))